package com.xebia.blogr;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations= {"classpath:blogr-main.xml"})
public class SpringContextLoaderTest {

	@Test
	public void shouldLoadSpringContextAndNotThrowErrors() {
		assertTrue(true);
	}
}
