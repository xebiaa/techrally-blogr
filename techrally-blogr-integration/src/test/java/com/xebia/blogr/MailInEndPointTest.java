package com.xebia.blogr;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import javax.mail.Address;
import javax.mail.internet.MimeMessage;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.integration.Message;

import com.xebia.blogr.mailextractor.BlogItem;
import com.xebia.blogr.mailextractor.MailInEndPoint;


@RunWith(MockitoJUnitRunner.class)
public class MailInEndPointTest {
	
	private MailInEndPoint mailInEndPoint;
	@Mock
	private Message<MimeMessage> message;

	private Address[] address = new Address[1];
	
	@Before
	public void setup() throws Exception {
		mailInEndPoint = new MailInEndPoint();
		address[0] = new Address() {
			
			@Override
			public String toString() {
				return "Xebia";
			}
			
			@Override
			public String getType() {
				return "address";
			}
			
			@Override
			public boolean equals(Object address) {
				return true;
			}
		};
	}
	
	@Test
	public void shouldInvokeMailInEndPoint() throws Exception {
		MimeMessage mimeMessage = mock(MimeMessage.class);
		
		given(mimeMessage.getFrom()).willReturn(address);
		given(mimeMessage.getSubject()).willReturn("hello world");
		given(message.getPayload()).willReturn(mimeMessage);
		
		BlogItem mail = mailInEndPoint.doHandleIncomingMail(message);
		assertThat(mail.getSubject(), is("hello world"));
	}
	
	
}
