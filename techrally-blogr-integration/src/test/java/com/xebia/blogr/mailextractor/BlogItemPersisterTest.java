package com.xebia.blogr.mailextractor;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.joda.time.DateTime;
import org.junit.Test;

import com.xebia.blogr.mongo.BlogRepository;


public class BlogItemPersisterTest {
	
	@Test
	public void testPersisteratification() throws Exception {
		BlogItem item = new BlogItem("title", new DateTime(), "author", "body");
		
		BlogRepository repo = mock(BlogRepository.class);
		when(repo.putJsonItem(isA(String.class))).thenReturn("success");
		BlogItemPersister persister = new BlogItemPersister();
		persister.setRepository(repo);
		assertThat(persister.persist(item), is("success"));
		verify(repo).putJsonItem(item.toJSON());
	}
}
