package com.xebia.blogr.mailextractor;

import static org.junit.Assert.assertTrue;

import org.joda.time.DateTime;
import org.junit.Test;


public class BlogItemTest {
	@Test
	public void shouldReturnLongString()
	{
		String body = "body contains a lot of possible characters";
		String author = "author";
		String subject = "subject";
		String tag = "tag";
		BlogItem item = new BlogItem(author, new DateTime(), subject, body, tag);
		String result = item.toJSON();
		assertTrue("The resulting JSON should contain more characters then the sum of it's parts",
				result.length() > (author + subject + body + tag).length());
		
	}

	@Test
	public void toJSONTest() {
		BlogItem blogitem = new BlogItem("schrijver", new DateTime(), "onderwerp", "Dit is alle tekst.");
		System.out.println(blogitem.toJSON());
	}
}
