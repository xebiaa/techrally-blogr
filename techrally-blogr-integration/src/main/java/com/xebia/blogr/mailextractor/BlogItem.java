package com.xebia.blogr.mailextractor;


import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.mail.BodyPart;
import javax.mail.Multipart;
import javax.mail.internet.MimeMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONException;
import org.json.JSONStringer;

import com.xebia.blogr.tagextractor.TagExtractor;
import com.xebia.blogr.tagextractor.TagExtractor.Noun;

/**
 * 
 */
public class BlogItem {
	
	public static DateTimeFormatter DATE_FORMAT = DateTimeFormat.forPattern("yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'");

	private static final Log LOG = LogFactory.getLog(BlogItem.class);
	
	private String author = "";
	private String subject;
	private DateTime date;

	private List<String> tags = new ArrayList<String>();
	private String body = "";

	public BlogItem(MimeMessage message, TagExtractor tagExtractor) throws MailInProcessingException {
		try {
			subject = message.getSubject();
			author = message.getFrom()[0].toString();
			Object messagePart = message.getContent();
			String content = "";
			if (messagePart instanceof String) {
				content = (String) messagePart;
				LOG.info("Got a message with body: " + content);
			} else if (messagePart instanceof Multipart) {
				LOG.info("This is a Multipart Message.  ");
				Multipart mp = (Multipart) messagePart;
				for (int j = 0; j < mp.getCount(); j++) {
					// Part are numbered starting at 0
					BodyPart b = mp.getBodyPart(j);
					Object o2 = b.getContent();
					if (o2 instanceof String) {
						content = (String) o2;
						LOG.info("Got content: " + content);
						break;
					} else if (o2 instanceof Multipart) {
						Multipart mp2 = (Multipart) o2;
					} else if (o2 instanceof InputStream) {
					}
				} // End of for
			}

			this.body = content;
			
			// add automatically extracted tags
			if (tagExtractor != null) {
				for (Noun noun : tagExtractor.extractNouns(content, 3)) {
					this.tags.add(noun.getWord());
				}
			}
			
		} catch (Exception e) {
			throw new MailInProcessingException(e);
		}
		date = new DateTime();
		validateBlogItem();
	}

	/**
	 * Initializing constructor, intended for unit testing.
	 * 
	 * @param author
	 *            the author.
	 * @param subject
	 *            the subject.
	 * @param body
	 *            the message body.
	 * @param tags
	 *            the tags (optional).
	 */
	BlogItem(String author, DateTime date, String subject, String body, String... tags) {
		super();
		this.author = author;
		this.subject = subject;
		this.body = body;
		this.tags = tags == null ? Collections.<String>emptyList() : Arrays.asList(tags);
		this.date = date;
	}

	private void validateBlogItem() throws MailInProcessingException {
		if (subject == null || subject.equals("")) {
			throw new MailInProcessingException("Subject is empty");
		}
	}

	public String toJSON() throws MailInProcessingException {
		String result;
		try {
			result = new JSONStringer().object()
				.key("datetime").value(DATE_FORMAT.print(getDate()))
				.key("author").value(getAuthor())
				.key("subject").value(getSubject())
				.key("tags").value(getTags())
				.key("body").value(getBody())
				.endObject().toString();
		} catch (JSONException e) {
			throw new MailInProcessingException(
					"Pronblem creating JSON message from MailItem: " + subject,
					e);
		}
		return result;
	}

	public String getAuthor() {
		return author;
	}

	public String getSubject() {
		return subject;
	}

	public List<String> getTags() {
		return tags;
	}

	public String getBody() {
		return body;
	}

	public DateTime getDate() {
		return date;
	}
}
