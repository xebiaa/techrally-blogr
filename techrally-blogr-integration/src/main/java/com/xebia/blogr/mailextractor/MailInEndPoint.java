package com.xebia.blogr.mailextractor;

import java.io.IOException;

import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;
import org.springframework.integration.Message;
import org.springframework.integration.annotation.ServiceActivator;

import com.xebia.blogr.tagextractor.TagExtractor;

/**
 * Test Mail endPoint to prove mails can be read from gmail.
 * 
 * @author albertsikkema
 * 
 */
public class MailInEndPoint {
	private static Logger LOG = Logger.getLogger(MailInEndPoint.class);
	
	private TagExtractor tagExtractor;
	
	public MailInEndPoint() {
		try {
			this.tagExtractor = new TagExtractor();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@ServiceActivator
	public BlogItem doHandleIncomingMail(Message<MimeMessage> message) throws Exception {
		MimeMessage email = message.getPayload();
		BlogItem item = new BlogItem(email, tagExtractor);
		LOG.info("Received an email with subject: " + email.getSubject());
		return item;
	}
}
