package com.xebia.blogr.mailextractor;

public class MailInProcessingException extends RuntimeException {
	
	public MailInProcessingException(String message) {
		super(message);
	}
	
	public MailInProcessingException(Throwable cause) {
		super(cause);
	}
	
	public MailInProcessingException(String message, Throwable cause) {
		super(message, cause);
	}

}
