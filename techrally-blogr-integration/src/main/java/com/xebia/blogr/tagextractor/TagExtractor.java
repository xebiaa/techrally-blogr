package com.xebia.blogr.tagextractor;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import opennlp.tools.lang.english.PosTagger;
import opennlp.tools.lang.english.SentenceDetector;
import opennlp.tools.lang.english.Tokenizer;
import opennlp.tools.postag.POSDictionary;

public class TagExtractor {
	
	private static final String PATH = "src/main/resources/";
	
	private static final String MODEL_NAME = "opennlp/model/english/sentdetect/EnglishSD.bin.gz";
	
	private static final String TAG_MODEL = "opennlp/model/english/postag/tag.bin.gz";
	private static final String TAG_DICT = "opennlp/model/english/postag/tagdict";

	private static final Object POS_NOUN = "NN";
	private static final Object POS_NOUNS = "NNS";

	private SentenceDetector sentenceDetector;
	private POSDictionary posDictionary;
	private PosTagger posTagger;
	private Tokenizer tokenizer;
	
	public TagExtractor() throws IOException {
		sentenceDetector = new SentenceDetector(PATH + MODEL_NAME);
		posDictionary = new POSDictionary(PATH + TAG_DICT);
		posTagger = new PosTagger(PATH + TAG_MODEL, posDictionary);
		tokenizer = new Tokenizer(PATH + MODEL_NAME);
	}
	
	public class Noun implements Comparable<Noun> {
		private String word;
		private int count;
		
		Noun(String word) {
			this.word = word;
		}
		
		@Override
		public int compareTo(Noun other) {
			int result = other.count - count;
			if (result == 0) {
				result = other.word.length() - word.length();
			}
			if (result == 0) {
				result = word.compareTo(other.word);
			}
			return result;
		}
		
		@Override
		public String toString() {
			return word + ":" + count;
		}

		public String getWord() {
			return word;
		}

		public int getCount() {
			return count;
		}
	}
	
	public SortedSet<Noun> extractNouns(String content, int threshold) {
		SortedSet<Noun> nouns = extractNouns(content);
		for (Iterator<Noun> it = nouns.iterator(); it.hasNext();) {
			if (it.next().count < threshold) {
				it.remove();
			}
		}
		return nouns;
	}
	
	public SortedSet<Noun> extractNouns(String content) {
		Map<String, Noun> nouns = new HashMap<String, Noun>();
		
		String[] sentences = sentenceDetector.sentDetect(content);
		for (String sentence : sentences) {
			String[] tokens = tokenizer.tokenize(sentence);
			String[] tags = posTagger.tag(tokens);
			int i = 0;
			for (String tag : tags) {
				String token = tokens[i++].replaceAll("[,\\.?!]", "");
//				System.out.println(token + "->" + tag);
				if (POS_NOUN.equals(tag) || POS_NOUNS.equals(tag)) {
					Noun noun = nouns.get(token);
					if (noun == null) {
						noun = new Noun(token);
						nouns.put(token, noun);
					}
					noun.count++;
				}
			}
		}

		return new TreeSet<Noun>(nouns.values());
	}
	
	public static void main(String[] args) throws IOException {
		String content = "The noun extractor should fully automatically extract nouns that occur at least 3 times in a sentence. "
				+ "We use OpenNLP to extract nouns from a sentence. We use OpenNLP to extract nouns from a sentence.";
		
		TagExtractor tagExtractor = new TagExtractor();
		SortedSet<Noun> nouns = tagExtractor.extractNouns(content, 2);
		for (Noun noun : nouns) {
			System.out.println(noun);
		}
	}
}
