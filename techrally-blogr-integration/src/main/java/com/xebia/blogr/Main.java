package com.xebia.blogr;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * This spring integration application can be run as a standalone jar file by
 * executing this main class.
 * 
 * @author albertsikkema
 * 
 */
public class Main {

	private static String loaderContextPath = "blogr-main.xml";

	public static void main(String[] args) {
		ClassPathXmlApplicationContext posterouxContext = new ClassPathXmlApplicationContext(loaderContextPath);
		posterouxContext.start();
	}
}
