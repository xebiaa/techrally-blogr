Techrally: noSQL and noJSP 
==========================

The Case
--------

Develop a microblogging application where posts can be made via email, translated into formatted content via a backend application
and posted on a html page. (like posterous). 

**WHY**

We like Posterous but we'd like a bit more control of the platform and its features. It's also a cool way to experiment
with some new technologies and ideas.

**GOAL(s)**

* Integrate the two hot-xebia-topics: nosql and nojsp combined with spring-integration and learn something in all these areas. 
* Everbody should be able to find his/her pick in this tech-stack to find something interesting to work on/with.
* A working product which we can extend and turn into something real in production. (A microblogging service which we use to post like crazy about cool technology).

Detailed instructions
---------------------
Spring-Integration polls from a mailbox (gmail) and handle all incoming mails in the INBOX with have a subject starting with post: 
Mails with no valid subject should be moved into the ERROR folder.
An email should be translated into a blog post by the following rules:

* Subject becomes the blog title
* Sender becomes author
* Body becomes the text of the post
* The Post is stored in mongoDB in the example format described below.
* After an email is processed it should be moved to a folder called PROCESSED.
* *(bonus)Emails should be handled asynchronously, we expect a lot of posts;-)*
* *(bonus)Images are inlined in the post*
* *(bonus)Attachments are added as download (with correct mime-header).*
* *(bonus)Links in the email are followed, their contents downloaded and migrated into a summary for the post. (asynchronously).*
* (optional)When an email subject starts with update: the existing post with the same title should be replaced by this post
* (optional)When an email subject starts with delete: the existing post with the same title should be removed

**A web application must be build which:**

* Exposes a REST (Spring MVC 3.0) service to serve json documents coming from mongoDB
* *(bonus) Implements session-less cookie based authentication [OAth](http://oauth.net/)*

The idea is that the main focus will be in the spring-integration corner and that we can keep adding more complexity here and that everybody can participate in whatever area is favoured. 

Technology stack
----------------

Here's the technology stack we want to use to build this:

* [Gmail](http://code.google.com/apis/gmail/oauth/)
* [MongoDb](http://www.springsource.org/spring-integration)
* [Spring Integration](http://www.springsource.org/spring-integration)
* [Spring MVC for REST](http://www.springsource.org/documentation)
* Html + javascript ([Jquery](http://jquery.com/), [Pure](http://beebole.com/pure/), [MooTools](http://mootools.net/), etc) 

Preparation
-----------
The following will be given when the techrally starts:

* A maven2 project which bootstraps spring integration.
* A gmail account for testing.
* Some test emails.
* A json format to start with.
* A maven2 war project with spring mvc 3.0 setup.


Examples
--------
**JSON format of a post**
	
    { "datetime" : "2010-12-23 17:59",  
      "author" : "asikkema@xebia.com",  
      "subject": "Android 2.2 makes developing for android a breeze",  
      "tags": ["android", "java"],  
      "body": "The android development platform has really matured since it early days. Setting up a development environment is only minutes and there is much more documetation"  
    }  
