package com.xebia.blogr.web;

import com.xebia.blogr.BlogItem;
import com.xebia.blogr.mongo.BlogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Collection;

/** @author mischa */
@Controller
public class ItemController {

    private BlogRepository repository;

    @Autowired
    public ItemController(BlogRepository repository) {
        this.repository = repository;
    }

    @RequestMapping(value = "/item/{id}", method = RequestMethod.GET)
    @ResponseBody
	public BlogItem item(@PathVariable("id") String id) {
		return repository.getItem(id);
	}

    /**
     * Find a collection that contains posts which have all of the comma separated list of tags
     */
    @RequestMapping(value = "/items/{tags}", method = RequestMethod.GET)
	public @ResponseBody Collection<BlogItem> items(@PathVariable String tags) {
        String[] tagList = null;

		if (tags != null) {
			tagList = tags.split("\\s*,\\s*");
		}
		return repository.getItems(tagList);
	}

    @RequestMapping(value = "/items", method = RequestMethod.GET)
    public @ResponseBody Collection<BlogItem> allItems() {
       return repository.getItems();
    }

}