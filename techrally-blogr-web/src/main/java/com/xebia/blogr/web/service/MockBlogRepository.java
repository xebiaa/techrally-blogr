package com.xebia.blogr.web.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import com.xebia.blogr.BlogItem;
import com.xebia.blogr.mongo.BlogRepository;
import com.xebia.blogr.mongo.MongoId;

/** @author mischa */
public class MockBlogRepository implements BlogRepository {
    @Override
    public BlogItem getItem(String id) {
        return new BlogItem(new MongoId(String.valueOf(id)), "subject", new Date(), "author", new HashSet(), "body");
    }

    @Override
    public List<BlogItem> getItems(String... tags) {
        List<BlogItem> items = new ArrayList<BlogItem>();
         items.add(new BlogItem(new MongoId(String.valueOf("1")), "subject", new Date(), "author", new HashSet(), "body"));
         items.add(new BlogItem(new MongoId(String.valueOf("2")), "subject", new Date(), "author", new HashSet(), "body"));
        return items;
    }

    @Override
    public String putJsonItem(String json) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
