package com.xebia.blogr.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Dummy controller (http://localhost:8080/techrally-blogr-web/blogr/ping)
 * 
 * @author albertsikkema
 * 
 */
@Controller
public class HelloWorldController {

	@RequestMapping(value = "/ping", method = RequestMethod.GET)
	@ResponseBody
	public String ping() {
		return "<html><body>pong</body></html>";
	}
}
