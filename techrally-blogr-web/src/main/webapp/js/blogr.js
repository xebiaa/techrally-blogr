$(function() {

    if ($('#post').size() > 0) {

        var parts = window.location.href.split('/');
        var id = parts[parts.length-1];

        //console.log('Found id ' + id);

        get_posting(id);
    }

    if ($('#listing').size() > 0) {
        get_postings();
    }


});




function get_posting(id) {
    $.getJSON('../blogr/item/' + id, function(data) {
          $('#subject').html(data.subject);
          $('#author').html(data.author);
          $('#body').html(data.body);
    });
}

function get_postings() {
    $.getJSON('blogr/items', function(data) {
        $(data).each(function(e, item) {
            $('#list section#posts')
                    .append($('<article>')
                    .attr('id', item.id).append($('<header>')
                    .text(item.subject).click(function(){window.location='post/'+item.id;}))
                    .append($('<section>').text(item.body))
                    .append($('<footer>').text(item.author)));
            
        });
       //$(data).each(function(e, m) {alert('kgk'); alert(m.subject);});
       //alert(data);
       //$('#list section').html(data);

    });
}

// vim:sw=4:et:ai
