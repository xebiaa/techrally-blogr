package com.xebia.blogr.web;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.servlet.HandlerAdapter;
import org.springframework.web.servlet.ModelAndView;

import static junit.framework.Assert.assertNotNull;

/**
 * 2010 Xebia B.V
 * User: frank
 * Created: Sep 17, 2010
 * Time: 1:53:05 PM
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("file:src/main/webapp/WEB-INF/blogr-servlet.xml")
public class ItemControllerSpringTest {

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private ItemController itemController;

    @Autowired
    private HandlerAdapter messageAdapter;
    
    private MockHttpServletResponse mockResponse;
    private MockHttpServletRequest mockRequest;

    @Before
    public void setup() {
        mockRequest = new MockHttpServletRequest();
        mockResponse = new MockHttpServletResponse();
    }

    @Test @Ignore
    public void call_ItemtoItemMethod() throws Exception {

        mockRequest.setRequestURI("/item/1");
        mockRequest.setMethod("GET");
        mockRequest.setContentType("application/json");
        ModelAndView mav = messageAdapter.handle(mockRequest, mockResponse, itemController);
        assertNotNull(mav);
    }

}
