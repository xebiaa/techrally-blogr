package com.xebia.blogr.web;

import com.xebia.blogr.mongo.BlogRepository;
import org.junit.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * @author Iwein Fuld
 */
public class ItemControllerTest {

	BlogRepository repository = mock(BlogRepository.class);

	ItemController itemController = new ItemController(repository);

	@Test
	public void shouldPassTags_multiple() {
		itemController.items("foo, bar");
		verify(repository).getItems("foo", "bar");
	}

	@Test
	public void shouldPassTags_single() {
		itemController.items("foo");
		verify(repository).getItems("foo");
	}

	@Test
	public void shouldPassTags_none() {
		itemController.items(null);
		verify(repository).getItems(null);
	}

}
