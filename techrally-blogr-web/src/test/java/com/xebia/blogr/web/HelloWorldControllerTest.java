package com.xebia.blogr.web;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.internal.builders.IgnoredClassRunner;
import org.junit.runner.RunWith;


@RunWith(IgnoredClassRunner.class)
public class HelloWorldControllerTest {

	
	@Test
	public void shouldInvokeHelloWorldController() throws Exception {
		assertThat(new HelloWorldController().ping(), is("<html><body>pong</body></html>"));
	}
}
