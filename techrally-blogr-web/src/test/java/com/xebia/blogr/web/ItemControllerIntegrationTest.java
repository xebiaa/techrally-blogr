package com.xebia.blogr.web;

import nl.flotsam.test.WebServer;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.internal.builders.IgnoredClassRunner;
import org.junit.runner.RunWith;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.IOException;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(IgnoredClassRunner.class)
public class ItemControllerIntegrationTest {

    @Rule
    public WebServer server = new WebServer(6767, this.getClass().getClassLoader(), new File(getBasedir(), "src/main/webapp"));

    @Test
    public void shouldReturnItems() throws IOException {
        RestTemplate template = new RestTemplate();
        ResponseEntity<String> result = template.getForEntity(server.getURL() + "blogr/items", String.class);
        assertThat(result.getStatusCode().value(), is(200));
        assertThat(result.getHeaders().getContentType().getType(), is("application"));
        assertThat(result.getHeaders().getContentType().getSubtype(), is("json"));
        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = mapper.readValue(result.getBody(), JsonNode.class);
        assertThat(node.isArray(), is(true));
    }

    @Test
    public void shouldReturnItem() throws IOException {
        RestTemplate template = new RestTemplate();
        ResponseEntity<String> result = template.getForEntity(server.getURL() + "blogr/item/4c936c7bc70d44db93586d9a", String.class);
        assertThat(result.getStatusCode().value(), is(200));
        assertThat(result.getHeaders().getContentType().getType(), is("application"));
        assertThat(result.getHeaders().getContentType().getSubtype(), is("json"));
        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = mapper.readValue(result.getBody(), JsonNode.class);
        assertThat(node.isArray(), is(false));
    }

    @Test
    public void shouldReturnItemsWhenPassingTags() throws IOException {
        RestTemplate template = new RestTemplate();
        ResponseEntity<String> result = template.getForEntity(server.getURL() + "blogr/items/foo,bar", String.class);
        assertThat(result.getStatusCode().value(), is(200));
        assertThat(result.getHeaders().getContentType().getType(), is("application"));
        assertThat(result.getHeaders().getContentType().getSubtype(), is("json"));
        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = mapper.readValue(result.getBody(), JsonNode.class);
        assertThat(node.isArray(), is(true));
    }

    private File getBasedir() {
        String basedir = System.getProperty("basedir");
        if (basedir != null) {
            return new File(basedir);
        } else {
            File file = new File(Thread.currentThread().getContextClassLoader().getResource(".").getPath());
            return file.getParentFile().getParentFile();
        }
    }

}
