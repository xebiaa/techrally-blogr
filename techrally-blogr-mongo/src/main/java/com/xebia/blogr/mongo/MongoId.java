package com.xebia.blogr.mongo;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

public class MongoId {
    private final String id;

    @JsonCreator
    public MongoId(@JsonProperty("$oid") String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
