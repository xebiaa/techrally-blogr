package com.xebia.blogr.mongo;

import com.xebia.blogr.BlogItem;

import java.util.List;

/**
 * @author Iwein Fuld
 * //TODO javadoc
 */
public interface BlogRepository {

	BlogItem getItem(String id);

	List<BlogItem> getItems(String ... tags);

	/**
	 *
	 * @param json the json to be stored
	 * @return the generated id
	 */
	String putJsonItem(String json);

}
