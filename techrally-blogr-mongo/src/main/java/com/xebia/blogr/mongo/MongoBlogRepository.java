package com.xebia.blogr.mongo;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.WriteResult;
import com.mongodb.util.JSON;
import com.xebia.blogr.BlogItem;

/**
 * @author Iwein Fuld
 *
 * TODO implement (testcases still missing)
 */
public class MongoBlogRepository implements BlogRepository{
	private final DB db;
    private DBCollection itemsCollection;

	/**
	 * Create a repository that connects to the given db
	 */
	public MongoBlogRepository(DB db) {
		this.db = db;
		itemsCollection = db.getCollection("items");
	}

	@Override
	public BlogItem getItem(String id) {
		DBObject result = itemsCollection.findOne(new ObjectId(id));

		return BlogItem.fromJson(result.toString());
	}

	@Override
	public List<BlogItem> getItems(String... tags) {
		List<BlogItem> items = new ArrayList<BlogItem>();
        DBCursor dbCursor = itemsCollection.find();

		while(dbCursor.hasNext()){
			DBObject dbObject = dbCursor.next();
			String json = dbObject.toString();

            items.add(BlogItem.fromJson(json));
		}

		return items;
	}

	@Override
	public String putJsonItem(String json) {
	    DBObject object = (DBObject) JSON.parse(json);

        WriteResult result = itemsCollection.save(object);

		ObjectId id = (ObjectId) object.get("_id");

        return id.toString();
	}
}
