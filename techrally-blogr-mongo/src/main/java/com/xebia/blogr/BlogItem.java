package com.xebia.blogr;

import java.io.IOException;
import java.util.Date;
import java.util.Set;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.ObjectMapper;

import com.xebia.blogr.mongo.MongoId;

/**
 * An immutable representation of a BlogItem that can be built from a json string.
 *
 * @author Iwein Fuld
 */
public class BlogItem {

	private final MongoId id;
	private String subject;
	private Date timestamp;
	private String author;
	private Set<String> tags;
	private String body;

	@JsonCreator
	public BlogItem(
			@JsonProperty("_id") MongoId id,
			@JsonProperty("subject") String subject,
			@JsonProperty("datetime") Date timestamp,
			@JsonProperty("author") String author,
			@JsonProperty("tags") Set<String> tags,
			@JsonProperty("body") String body
	) {
		this.subject = subject;
		this.timestamp = timestamp;
		this.author = author;
		this.tags = tags;
		this.body = body;
		this.id = id;
	}

	/**
	 * Deserialize the given json into a BlogItem object
	 */
	public static BlogItem fromJson(String json) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.readValue(json, BlogItem.class);
		} catch (IOException e) {
			throw new RuntimeException("Failed to read json string", e);
		}
	}

    public String getAuthor() {
        return author;
    }

    public String getBody() {
        return body;
    }

    public String getId() {
        return id.getId();
    }

    public String getSubject() {
        return subject;
    }

    public Set<String> getTags() {
        return tags;
    }

    public Date getTimestamp() {
        return timestamp;
    }
}
