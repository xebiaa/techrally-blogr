package com.xebia.blogr;

import static org.hamcrest.Matchers.*;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author Iwein Fuld
 */

public class BlogItemTest {

	@Test
	public void shouldBuildBlogItem() {
		BlogItem item = BlogItem.fromJson(itemAsJson());
		assertThat(item.getAuthor(), is("asikkema@xebia.com"));
		assertThat(item.getBody(), is("This is the body of an item"));
		assertThat(item.getSubject(), containsString("2.2"));
		assertThat(item.getTags(), hasItems("android", "java"));
	}

	private String itemAsJson() {
		return "{ \"_id\" : { \"$oid\" : \"4c9368d6f1b99dcb2ecda6ca\"}, \n" +
				" \"datetime\" : \"2010-12-23T17:59:00.000Z\", \n" +
				" \"author\" : \"asikkema@xebia.com\", \n" +
				" \"subject\": \"Android 2.2 makes developing for android a breeze\", \n" +
				" \"tags\": [\"android\", \"java\"], \n" +
				" \"body\": \"This is the body of an item\" \n" +
				"}";
	}
}
