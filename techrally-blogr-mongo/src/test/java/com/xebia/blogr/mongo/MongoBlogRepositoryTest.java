package com.xebia.blogr.mongo;

import com.xebia.blogr.BlogItem;
import com.xebia.mongo.test.TemporaryMongoDb;
import org.junit.Rule;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

/**
 * @author Iwein Fuld
 */
public class MongoBlogRepositoryTest {

	@Rule
	public TemporaryMongoDb tempDb = new TemporaryMongoDb("localhost", "blogr-") {
		@Override
		protected void before() throws Throwable {
			super.before();
			repository = new MongoBlogRepository(tempDb.getDB());
		}
	};
	
	private MongoBlogRepository repository;

	@Test
	public void shouldGetItemByIdFromMongo() {
	    String id = repository.putJsonItem(itemAsJson("a body"));

	    assertNotNull(id);

	    BlogItem item = repository.getItem(id);

	    assertNotNull(item);
	    assertEquals(id, item.getId());
	    assertEquals("a body", item.getBody());
	}

	@Test
	public void shouldGetAllItemsWeInsertFromMongo() {
	    String id1 = repository.putJsonItem(itemAsJson("a body"));
	    String id2 = repository.putJsonItem(itemAsJson("another body"));

	    assertNotNull(id1);
	    assertNotNull(id2);
	    assertFalse(id1.equals(id2));

		List<BlogItem> items = repository.getItems();

        assertThat(items.size(), is(2));
        assertTrue(items.get(0).getId().equals(id1) || items.get(0).getId().equals(id2));
        assertTrue(items.get(1).getId().equals(id1) || items.get(1).getId().equals(id2));
	}

    private String itemAsJson(String body) {
        return "{ \"datetime\" : \"2010-12-23T17:59:00.000Z\",  \n" +
        		"  \"author\" : \"asikkema@xebia.com\",  \n" +
        		"  \"subject\": \"Android 2.2 makes developing for android a breeze\",  \n" +
        		"  \"tags\": [\"android\", \"java\"],  \n" +
        		"  \"body\": \"" + body + "\"  \n" +
        		"}";
    }

}
